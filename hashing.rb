require 'digest'

# code provided by mozilla (https://github.com/mozilla/openbadges/wiki/How-to-hash-&-salt-in-various-languages.)
def hashEmailAddress(email, salt)
  'sha256$' + Digest::SHA256.hexdigest(email + salt)
end

def testHash()
a = {}
puts "Email?"
email = gets.chomp().to_s
puts "Salt?"
salt = gets.chomp().to_s
hash = hashEmailAddress(email,salt)
a[salt] = hash
puts "Salt: #{salt} Value: #{a[salt]}"
end

testHash()